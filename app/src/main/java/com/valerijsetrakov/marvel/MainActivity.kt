package com.valerijsetrakov.marvel

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.valerijsetrakov.marvel.data.marvel.api.MarvelApi
import com.valerijsetrakov.marvel.data.marvel.common.Character
import com.valerijsetrakov.marvel.data.marvel.common.ComicSummary
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val api: MarvelApi = MarvelApi.create()
        val character = ComicSummary("sd", "b")
        println("character $character")
        val disposable = api.characters()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        { Log.d(MainActivity::class.java.simpleName, it.toString()) },
                        { Log.e(MainActivity::class.java.simpleName,"", it) }
                )
    }
}
