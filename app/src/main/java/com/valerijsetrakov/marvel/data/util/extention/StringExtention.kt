package com.valerijsetrakov.marvel.data.util.extention

import java.security.MessageDigest

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return md.digest(toByteArray()).joinToString("") {
        String.format("%02x", it)
    }
}