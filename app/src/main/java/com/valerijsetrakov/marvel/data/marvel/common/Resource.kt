package com.valerijsetrakov.marvel.data.marvel.common

import java.util.*

abstract class Resource (
        val id: Int,
        val title: String,
        val description: String,
        val resourceURI: String,
        val urls: Array<Url>,
        val modified: Date,
        val thumbnail: Image)

class Character (
        id: Int,
        title: String,
        description: String,
        resourceURI: String,
        urls: Array<Url>,
        modified: Date,
        thumbnail: Image,
        val comics: ComicList,
        val stories: StoryList,
        val events: EventList,
        val series: SeriesList)
    : Resource(id, title, description, resourceURI, urls, modified, thumbnail)

open class Comics (
        id: Int,
        title: String,
        description: String,
        resourceURI: String,
        urls: Array<Url>,
        modified: Date,
        thumbnail: Image,
        val digitalId: Int,
        val issueNumber: Int,
        val variantDescription: String,
        val isbn: String,
        val upc: String,
        val diamondCode: String,
        val ean: String,
        val issn: String,
        val format: String,
        val pageCount: Int,
        val textObjects: Array<TextObject>,
        val series: SeriesSummary,
        val variants: Array<ComicSummary>,
        val collections: Array<ComicSummary>,
        val collectedIssues: Array<ComicSummary>,
        val dates: Array<ComicDate>,
        val prices: Array<ComicPrice>,
        val images: Array<Image>,
        val creators: CreatorList,
        val characters: CharacterList,
        val stories: StoryList,
        val events: EventList)
    :Resource(id, title, description, resourceURI, urls, modified, thumbnail)

open class Event (
        id: Int,
        title: String,
        description: String,
        resourceURI: String,
        urls: Array<Url>,
        modified: Date,
        thumbnail: Image,
        val start: Date,
        val end: Date,
        val comics: ComicList,
        val stories: StoryList,
        val series: SeriesList,
        val characters: CharacterList,
        val creators: CreatorList,
        val next: EventSummary,
        val previous: EventSummary)
    : Resource(id, title, description, resourceURI, urls, modified, thumbnail)

open class Serie (
        id: Int,
        title: String,
        description: String,
        resourceURI: String,
        urls: Array<Url>,
        modified: Date,
        thumbnail: Image,
        val startYear: Int,
        val endYear: Int,
        val rating: String,
        val comics: ComicList,
        val stories: SeriesList,
        val events: EventList,
        val characters: CharacterList,
        val creators: CreatorList,
        val next: SeriesSummary,
        val previous: SeriesSummary)
    :Resource(id, title, description, resourceURI, urls, modified, thumbnail)

open class Creator (
        val id: Int,
        val firstName: String,
        val middleName: String,
        val lastName: String,
        val suffix: String,
        val fullName: String,
        val modified: Date,
        val resourceURI: String,
        val urls: Array<Url>,
        val thumbnail: Image,
        val series: SeriesList,
        val stories: StoryList,
        val comics: ComicList,
        val events: EventList
)