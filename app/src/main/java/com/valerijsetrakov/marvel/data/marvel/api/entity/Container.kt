package com.valerijsetrakov.marvel.data.marvel.api.entity

data class Container<T> (val offset: Int,
                         val limit: Int,
                         val total: Int,
                         val count: Int,
                         val results: Array<T>)