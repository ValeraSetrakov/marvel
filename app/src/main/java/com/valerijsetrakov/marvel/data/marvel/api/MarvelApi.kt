package com.valerijsetrakov.marvel.data.marvel.api

import com.valerijsetrakov.marvel.data.marvel.api.entity.Response
import com.valerijsetrakov.marvel.data.marvel.common.Character
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface MarvelApi {

    @GET("characters")
    fun characters(): Single<Response<Character>>

    /**
     * Companion object to create the GithubApiService
     */
    companion object {
        fun create(): MarvelApi {
            val okHttpClient = OkHttpClient.Builder().addInterceptor(ApiKeyInterceptor()).build()
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .baseUrl("https://gateway.marvel.com/v1/public/")
                    .build()

            return retrofit.create(MarvelApi::class.java)
        }
    }
}