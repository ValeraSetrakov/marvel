package com.valerijsetrakov.marvel.data.marvel.api

import com.valerijsetrakov.marvel.PrivateConstant
import com.valerijsetrakov.marvel.data.util.extention.md5
import okhttp3.Interceptor
import okhttp3.Response
import java.util.*

class ApiKeyInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val ts = Date().time.toString()
        val publicKey = PrivateConstant.PUBLIC_KEY
        val privateKey = PrivateConstant.PRIVATE_KEY
        val hash = "$ts$privateKey$publicKey".md5().toLowerCase()
        val newUrl = request
                .url()
                .newBuilder()
                .addQueryParameter("apikey",publicKey)
                .addQueryParameter("ts", ts)
                .addQueryParameter("hash", hash)
                .build()
        val newRequestBuilder = request.newBuilder().url(newUrl)
        val newRequest = newRequestBuilder.build()
        return chain.proceed(newRequest)
    }
}