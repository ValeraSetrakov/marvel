package com.valerijsetrakov.marvel.data.marvel.common

abstract class ResourceList<T> (val available: Int, val returned: Int, val collectionURI: String, val items: Array<T>)

class ComicList(available: Int, returned: Int, collectionURI: String, items: Array<ComicSummary>)
    :ResourceList<ComicSummary>(available, returned, collectionURI, items)

class StoryList(available: Int, returned: Int, collectionURI: String, items: Array<StorySummary>)
    :ResourceList<StorySummary>(available, returned, collectionURI, items)

class EventList(available: Int, returned: Int, collectionURI: String, items: Array<EventSummary>)
    :ResourceList<EventSummary>(available, returned, collectionURI, items)

class SeriesList(available: Int, returned: Int, collectionURI: String, items: Array<SeriesSummary>)
    :ResourceList<SeriesSummary>(available, returned, collectionURI, items)

class CreatorList(available: Int, returned: Int, collectionURI: String, items: Array<CreatorSummary>)
    :ResourceList<CreatorSummary>(available, returned, collectionURI, items)

class CharacterList(available: Int, returned: Int, collectionURI: String, items: Array<CharacterSummary>)
    :ResourceList<CharacterSummary>(available, returned, collectionURI, items)