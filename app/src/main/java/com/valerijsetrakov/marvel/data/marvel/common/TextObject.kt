package com.valerijsetrakov.marvel.data.marvel.common

data class TextObject(val type: String, val language: String, val text: String)