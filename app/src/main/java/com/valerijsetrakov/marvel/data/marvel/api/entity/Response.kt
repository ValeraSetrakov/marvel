package com.valerijsetrakov.marvel.data.marvel.api.entity

data class Response<T> (
        val code: Int,
        val status: String,
        val data: Container<T>,
        val etag: String,
        val copyright: String,
        val attributionText: String,
        val attributionHTML: String)