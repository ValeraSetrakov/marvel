package com.valerijsetrakov.marvel.data.marvel.common

abstract class ResourceSummary (val resourceURI: String, val name: String)

class StorySummary(resourceURI: String, name: String, val type: String): ResourceSummary(resourceURI, name)

class ComicSummary(resourceURI: String, name: String): ResourceSummary(resourceURI, name)

class EventSummary(resourceURI: String, name: String): ResourceSummary(resourceURI, name)

class SeriesSummary(resourceURI: String, name: String): ResourceSummary(resourceURI, name)

class CreatorSummary(resourceURI: String, name: String, val role: String): ResourceSummary(resourceURI, name)

class CharacterSummary(resourceURI: String, name: String, val role: String): ResourceSummary(resourceURI, name)