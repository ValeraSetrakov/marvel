package com.valerijsetrakov.marvel.data.marvel.common

import java.util.*

abstract class ResourceDate(val type: String, val date: Date)

open class ComicDate (type: String, date: Date) : ResourceDate(type, date)