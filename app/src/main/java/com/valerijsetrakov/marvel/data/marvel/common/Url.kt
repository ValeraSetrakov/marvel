package com.valerijsetrakov.marvel.data.marvel.common

data class Url (val type: String, val url: String)