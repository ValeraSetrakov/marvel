package com.valerijsetrakov.marvel.data.marvel.common

data class Image (val path: String, val extension: String)