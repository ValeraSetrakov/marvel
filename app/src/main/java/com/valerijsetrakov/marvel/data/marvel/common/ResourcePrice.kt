package com.valerijsetrakov.marvel.data.marvel.common

abstract class ResourcePrice(val type: String, val price: Float)

open class ComicPrice (type: String, price: Float) : ResourcePrice(type, price)